﻿using LAB04.Models;
using LAB04.Repotr.StudentReport;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAB04
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            StudentContexDB db = new StudentContexDB();

            var listStudentReportDto = db.Students.Select(c => new StudentReportDto
            {
                StudentID = c.StudentID,
                FullName = c.FullName,
                AverageScore = c.AverageScore,
                FacultyName = c.Faculty.FacultyName,

            }).ToList();

            this.reportViewer1.LocalReport.ReportPath = "rptStudentReport.rdlc";
            var reportDataSource = new ReportDataSource("StudentReportDataSet", listStudentReportDto);
            this.reportViewer1.LocalReport.DataSources.Clear(); //clear
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);


            this.reportViewer1.RefreshReport();
        }
    }
}
